export default {
  Host: "http://api.luffycity.cn:8000",
  token(){
    return localStorage.user_token || sessionStorage.user_token;
  }
}
