import Vue from "vue"
import Router from "vue-router"
import Home from '../components/Home'
import Login from '../components/Login'
import Register from '../components/Register'
import Course from '../components/Course'
import Detail from "../components/Detail";
import Cart from "../components/cart";

// 这里导入可以让让用户访问的组件

Vue.use(Router);

export default new Router({
  // 设置路由模式为‘history’，去掉默认的#
  mode: "history",
  routes:[
    // 路由列表
    {
      path: '/',
      name: 'Home',
      component:Home,
    }, {
      path: '/user/login',
      name: 'Login',
      component:Login,
    },
    {
      path: '/user/register',
      name: 'Register',
      component:Register,
    },
    {
      path: '/courses',
      name: 'Course',
      component:Course,
    },
    {
      path: '/courses/:id',
      name: 'Detail',
      component:Detail,
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart,
    },
  ]
})




