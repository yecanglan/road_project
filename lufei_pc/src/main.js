// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './routers/index';

Vue.config.productionTip = false

import settings from "./settings";
Vue.prototype.$settings = settings;

// 注册elementui到项目中
import ElementUI from 'element-ui';
import "element-ui/lib/theme-chalk/index.css"
Vue.use(ElementUI);

// 加载项目的初始化css样式
import "../static/css/reset.css";

// axios
import axios from "axios"
// 允许ajax发送请求时附带cookie
axios.defaults.withCredentials = false;

Vue.prototype.$axios = axios; // 把对象挂载vue中

// 导入极验验证码核心js文件
import '../static/js/gt';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});

require('video.js/dist/video-js.css');
require('vue-video-player/src/custom-theme.css');
import VideoPlayer from 'vue-video-player'
Vue.use(VideoPlayer);
