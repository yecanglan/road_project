from celery import Celery

# 初始化celery应用对象
app = Celery("luffy")

# ! 1.1 如果celery需要在任务调用其他框架的内部对象,则需要进行相应的框架初始化
import os
import django
os.environ.update({'DJANGO_SETTINGS_MODULE': 'luffyapi.settings.dev'})
django.setup()  # django的初始化

# 加载初始化配置
app.config_from_object("mycelery.config")

# 注册异步任务
# app.autodiscover_tasks(["任务1", "任务2"])
app.autodiscover_tasks(["mycelery.sms","mycelery.email"])

# 在终端启动celery
# celery -A mycelery.main worker -l info