from luffyapi.utils.models import models, BaseModel
# Create your models here.

class Banner(BaseModel):
    # 字段声明
    image = models.ImageField(upload_to="banner", null=True, blank=True, verbose_name="轮播图片")
    link = models.CharField(max_length=500, verbose_name="广告链接")
    name = models.CharField(max_length=250, verbose_name="广告标题")
    note = models.CharField(max_length=150,null=True, blank=True, verbose_name='备注信息')


    # 表信息
    class Meta:
        db_table = "ly_banner"
        verbose_name = "轮播广告"
        verbose_name_plural = verbose_name # 避免出现多个复数数据时，自动加"s"，设置复数和单数一致

    # 自定义字段/查询方法
    def __str__(self):
        return self.name

class Nav(BaseModel):
    """导航菜单"""
    POSITION_CHOICES = (
        (0, "顶部导航"),
        (1, "脚部导航"),
    )
    name = models.CharField(max_length=50, verbose_name="导航名称")
    link = models.CharField(max_length=500, verbose_name="导航链接")
    position = models.SmallIntegerField(default=0, choices=POSITION_CHOICES, verbose_name="导航位置")
    is_http = models.BooleanField(default=False, verbose_name="是否是站外地址")

    class Meta:
        db_table = "ly_nav"
        verbose_name = "导航菜单"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name