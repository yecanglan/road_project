from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser
class User(AbstractUser):
    mobile = models.CharField(max_length=20, unique=True, verbose_name='手机号')
    avatar = models.ImageField(upload_to="avatar", null=True, blank=True, verbose_name="头像")
    wechat = models.CharField(max_length=100, null=True, blank=True, verbose_name="微信号")
    class Meta:
        db_table = 'ly_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name