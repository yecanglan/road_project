from rest_framework.views import APIView
from luffyapi.libs.geetest import GeetestLib
from rest_framework.response import Response


# Create your views here.

class GeetestCapchaAPRView(APIView):
    '''生成验证码的流水号'''
    def get(self, request):
        user_id = 'test'
        gt = GeetestLib(settings.GEETEST["pc_geetest_id"], settings.GEETEST["pc_geetest_key"])
        status = gt.pre_process(user_id)
        response_str = gt.get_response_str()
        import json
        response = json.loads(response_str)
        return Response(response)
    def post(self,request):
        '''校验验证码的结果'''
        if request.method == "POST":
            gt = GeetestLib(settings.GEETEST["pc_geetest_id"], settings.GEETEST["pc_geetest_key"])
            challenge = request.data.get(gt.FN_CHALLENGE, '')
            validate = request.data.get(gt.FN_VALIDATE, '')
            seccode = request.data.get(gt.FN_SECCODE, '')
            result = gt.failback_validate(challenge, validate, seccode)
            result = {"status": "success"} if result else {"status": "fail"}
            return Response(result)


'''
模型-->视图--->序列化器-->路由
'''
from rest_framework.generics import CreateAPIView
from .models import User
from .serializer import UserModelSerializer
class UserAPIView(CreateAPIView):
    '''用户注册'''
    queryset = User.objects.all()
    serializer_class = UserModelSerializer


'''
url:
    /users/sms/(?P<mobile>1[3-9]\d{9})/
'''
from rest_framework import status
import random
from django_redis import get_redis_connection
from django.conf import settings
class SMSAPIView(APIView):
    def get(self, request, mobile):
        """
        短信发送接口
        url:
           /users/sms/(?P<mobile>1[3-9]\d{9})/
        """
        # 1. 验证手机号码是否已经注册了
        try:
            User.objects.get(mobile=mobile)
            return Response({"message": "对不起，当前手机已经被注册！"}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            pass

        #2. 验证手机是否在一分钟内曾经发送过短信了
        redis_conn = get_redis_connection("sms_code")
        ret = redis_conn.get("interval_%s" % mobile)
        if ret is not None:
            return Response({"message": "对不起，短信发送过于频繁！"})

        #3. 生成短信验证码
        sms_code = "%06d" % random.randint(100, 999999)

        #4. 保存短信验证码
        # redis_conn.setex("键","时间","值")
        # 使用redis提供的事务配合管道[pipeline]操作来保证多条命令要么一起执行,要么一起失败
        # redis事务只能控制数据的修改,设置和删除,对于获取数据来说,没必要使用事务
        pipe = redis_conn.pipeline()  # 创建一个管道
        pipe.multi()  # 开启redis事务
        redis_conn.setex("sms_%s" % mobile, settings.SMS["sms_expire_time"], sms_code)
        redis_conn.setex("interval_%s" % mobile, settings.SMS["sms_interval_time"], "_")
        pipe.execute()  # 执行redis事务

        #5. 发送短信
        # 调用celery的异步任务发送短信!
        from mycelery.sms.tasks import send_sms_code
        send_sms_code.delay(mobile, sms_code)

        result = "短信正在发送短信,请留意您的手机."
        return Response({"message": result})

