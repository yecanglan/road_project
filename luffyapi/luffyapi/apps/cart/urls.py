from django.urls import path
from . import views
urlpatterns = [
    path("", views.CartAPIView.as_view({
        "post": "add_cart",
        "get": "get_cart",
        "put": "change_selected_status",
        'delete': 'delete_cart',
        'patch': 'change_expire',
    })),
]