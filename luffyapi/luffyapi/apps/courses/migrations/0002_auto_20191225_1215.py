# Generated by Django 2.2.8 on 2019-12-25 04:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='courselesson',
            name='lesson',
        ),
        migrations.AlterField(
            model_name='teacher',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='teacher', verbose_name='讲师封面'),
        ),
    ]
