from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListAPIView,RetrieveAPIView
from .models import CourseCategory
from .serializers import CourseCategoryModelSerializer
"""模型->视图->序列化器->路由"""
class CourseCategoryListAPIView(ListAPIView):
    """课程分类列表"""
    queryset = CourseCategory.objects.filter(is_show=True,is_delete=False).order_by("orders", "-id")
    serializer_class = CourseCategoryModelSerializer

from .models import Course
from .serializers import CourseModelSerializer,CourseRetrieveSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from .paginations import CourseListPagination
class CourseListAPIView(ListAPIView):
    """课程列表"""
    queryset = Course.objects.filter(is_show=True,is_delete=False).order_by("orders", "id")
    serializer_class = CourseModelSerializer
    filter_backends = [DjangoFilterBackend,OrderingFilter]
    filter_fields = ["course_category"]
    ordering_fields = ["id","students","price"]
    pagination_class = CourseListPagination

class CourseRetrieveAPIView(RetrieveAPIView):
    '''课程详情接口'''
    queryset = Course.objects.filter(is_delete=False, is_show=True)
    serializer_class = CourseRetrieveSerializer

from .models import CourseChapter
from .serializers import CourseChapterModelSerializer
class CourseChapterListAPIView(ListAPIView):
    queryset = CourseChapter.objects.filter(is_show=True, is_delete=False).order_by("chapter")
    serializer_class = CourseChapterModelSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['course', ]


