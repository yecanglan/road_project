from rest_framework import serializers
from .models import CourseCategory
class CourseCategoryModelSerializer(serializers.ModelSerializer):
    """课程分类信息的序列化器"""

    class Meta:
        model = CourseCategory
        fields = ["id","name"]

from .models import Course,Teacher
class TeacherModelSerializer(serializers.ModelSerializer):
    """老师的序列化器"""
    class Meta:
        model = Teacher
        fields = ["id","name","role","title","signature","brief","image"]

class CourseModelSerializer(serializers.ModelSerializer):
    """课程信息的序列化器"""
    # 序列化器嵌套
    teacher = TeacherModelSerializer() # 嵌套单个
    # teacher_list = TeacherModelSerializer(many=True) # 嵌套多个
    class Meta:
        model = Course
        fields = ["id", "name", "course_img", "students", "lessons", "pub_lessons", "price", "teacher", "free_lesson_list", "discount_name", "discount_price"]
class CourseRetrieveSerializer(serializers.ModelSerializer):
    '''课程详情页的序列化器'''
    teacher = TeacherModelSerializer()

    class Meta:
        model = Course
        fields = ["id", "name", "course_img", "students", "lessons", "course_video", "pub_lessons", "price", "teacher", "brief",
                  "level_name", "discount_name", "discount_price", "has_time"]

from .models import CourseChapter,CourseLesson
class CourseLessonModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseLesson
        fields = ["id", "name", "lesson", "free_trail", "duration"]

class CourseChapterModelSerializer(serializers.ModelSerializer):
    '''课程章节的序列化器'''
    lessons = CourseLessonModelSerializer(many=True)
    class Meta:
        model = CourseChapter
        fields = ["chapter", "name", "summary", "lessons"]